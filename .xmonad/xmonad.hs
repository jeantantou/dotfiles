-- XMONAD - CONFIGURATION PERSONNELLE

-------------------------------------
-- IMPORTS
-------------------------------------
    -- Base
import XMonad
import XMonad.Config.Bepo
import XMonad.Config.Desktop
import System.IO (hPutStrLn)
import System.Exit (exitSuccess)
import qualified XMonad.StackSet as W

    -- Actions
import XMonad.Actions.CopyWindow (kill1, copyToAll, killAllOtherCopies, runOrCopy)
import XMonad.Actions.CycleWS (Direction1D(..), moveTo, shiftTo, WSType(..), nextScreen, prevScreen, shiftNextScreen, shiftPrevScreen)
-- import XMonad.Actions.DynamicWorkspaces (addWorkspacePrompt, removeEmptyWorkspace)
import XMonad.Actions.GridSelect
import XMonad.Actions.Minimize (minimizeWindow)
import XMonad.Actions.MouseResize
import XMonad.Actions.Promote
import XMonad.Actions.RotSlaves (rotSlavesDown, rotAllDown)
import qualified XMonad.Actions.TreeSelect as TS
import XMonad.Actions.WindowGo (runOrRaise, raiseMaybe)
import XMonad.Actions.WithAll (sinkAll, killAll)
import qualified XMonad.Actions.Search as S
-- import qualified XMonad.Actions.ConstrainedResize as Sqr

    -- Data
import Data.Char (isSpace)
import Data.List
import Data.Monoid
import Data.Maybe (isJust, fromJust)
import Data.Tree
import qualified Data.Map as M

    -- Hooks
import XMonad.Hooks.DynamicLog (dynamicLogWithPP, defaultPP, wrap, pad, xmobarPP, xmobarColor, shorten, PP(..))
import XMonad.Hooks.EwmhDesktops -- require for xcomposite in order to work
import XMonad.Hooks.ManageDocks (docks, avoidStruts, docksStartupHook, docksEventHook, manageDocks, ToggleStruts(..))
import XMonad.Hooks.ManageHelpers (isFullscreen, isDialog,  doFullFloat, doCenterFloat)
-- import XMonad.Hooks.Place (placeHook, withGaps, smart)
import XMonad.Hooks.ServerMode
import XMonad.Hooks.SetWMName
import XMonad.Hooks.UrgencyHook
import XMonad.Hooks.WorkspaceHistory

    -- Layouts
import XMonad.Layout.Fullscreen (fullscreenFull, fullscreenSupport)
import XMonad.Layout.GridVariants (Grid(Grid))
import XMonad.Layout.IM (withIM, Property(Role))
import XMonad.Layout.OneBig
import XMonad.Layout.ResizableTile
import XMonad.Layout.SimplestFloat
import XMonad.Layout.Tabbed
import XMonad.Layout.ThreeColumns
import XMonad.Layout.ZoomRow (zoomRow, zoomIn, zoomOut, zoomReset, ZoomMessage(ZoomFullToggle))

    -- Layout Modifiers
import XMonad.Layout.LayoutModifier
import XMonad.Layout.LimitWindows (limitWindows, increaseLimit, decreaseLimit)
import XMonad.Layout.MultiToggle (mkToggle, single, EOT(EOT), Toggle(..), (??))
import XMonad.Layout.MultiToggle.Instances (StdTransformers(NBFULL, MIRROR, NOBORDERS))
import XMonad.Layout.NoBorders
import XMonad.Layout.PerWorkspace (onWorkspace)
import XMonad.Layout.Reflect (reflectVert, reflectHoriz, REFLECTX(..), REFLECTY(..))
import XMonad.Layout.Renamed (renamed, Rename(CutWordsLeft, Replace))
import XMonad.Layout.Spacing (spacing)
import XMonad.Layout.ShowWName
import XMonad.Layout.Simplest
import XMonad.Layout.SubLayouts
import XMonad.Layout.WorkspaceDir
import XMonad.Layout.WindowArranger (windowArrange, WindowArrangerMsg(..))
import qualified XMonad.Layout.ToggleLayouts as T (toggleLayouts, ToggleLayout(Toggle))
import qualified XMonad.Layout.MultiToggle as MT (Toggle(..))

    -- Utilities
import XMonad.Util.Dmenu
import XMonad.Util.EZConfig (additionalKeysP, additionalMouseBindings)
import XMonad.Util.Loggers
import XMonad.Util.NamedScratchpad
import XMonad.Util.NamedWindows
import XMonad.Util.Run (runProcessWithInput, safeSpawn, unsafeSpawn, runInTerm, spawnPipe)
import XMonad.Util.Scratchpad
import XMonad.Util.SpawnOnce

--------------------------------------
-- CONFIG
--------------------------------------
myFont          = "xft:Mononoki Nerd Font:regular:pixelsize=12"
myModMask       = mod4Mask
myTerminal      = "st"
myTextEditor    = "nvim"
myBorderWidth   = 1 -- in pixels
altMask         = mod1Mask
windowCount     = gets $ Just . show . length . W.integrate' . W.stack . W.workspace . W.current . windowset


main :: IO ()
main = do
  xmproc <- spawnPipe "xmobar -x 0 /home/quentin/.config/xmobar/xmobarrc.hs" -- le 0 indique de ne lancer xmobar que sur le priemier écran
  xmonad
    $ withUrgencyHook LibNotifyUrgencyHook
    $ docks bepoConfig
    { manageHook = ( isFullscreen --> doFullFloat ) <+> myManageHook <+> manageHook bepoConfig <+> manageDocks <+> manageScratchPad
    , handleEventHook = serverModeEventHookCmd
                        -- for real fullscreen
                        <+> XMonad.Hooks.EwmhDesktops.fullscreenEventHook
                        <+> serverModeEventHook
                        <+> serverModeEventHookF "XMONAD_PRINT" (io . putStrLn)
                        <+> docksEventHook
    , logHook = dynamicLogWithPP xmobarPP
                { ppOutput = \x -> hPutStrLn xmproc x
                , ppCurrent = xmobarColor "#c3e88d" "" . wrap "[" "]" -- Current workspace in xmobar
                , ppVisible = xmobarColor "#c3e88d" ""                -- Visible but not current workspace
                , ppHidden = xmobarColor "#82AAFF" "" . wrap "*" "" . (\ws -> if ws == "NSP" then "" else ws)  -- Hidden workspaces in xmobar
                , ppHiddenNoWindows = xmobarColor "#F07178" ""        -- Hidden workspaces (no windows)
                , ppTitle = xmobarColor "#d0d0d0" "" . shorten 60     -- Title of active window in xmobar
                , ppSep =  "<fc=#666666> | </fc>"                     -- Separators in xmobar
                , ppUrgent = xmobarColor "#C45500" "" . wrap "!" "!"  -- Urgent workspace
                , ppExtras  = [windowCount]                           -- # of windows current workspace
                , ppOrder  = \(ws:l:t:ex) -> [ws,l]++ex++[t]
                }
    , modMask = myModMask
    , terminal = myTerminal
    , startupHook = myStartupHook
    , layoutHook = myLayoutHook
    , borderWidth = myBorderWidth
    }
    `additionalKeysP` myKeys

-------------------------------------------
-- AUTOSTART
-------------------------------------------
myStartupHook :: X ()
myStartupHook = do
  setWMName "LG3D"
  spawnOnce "xrdb .Xressources"
  spawnOnce "setxkbmap fr bepo"
  spawnOnce "nitrogen --restore &"
  -- spawnOnce "~/.fehbg &"  -- set last saved feh wallpaper
  spawnOnce "/usr/bin/emacs --daemon &"

-------------------------------------------
-- GRID SELECT
-------------------------------------------
myColorizer :: Window -> Bool -> X (String, String)
myColorizer = colorRangeFromClassName
              (0x28,0x2c,0x34) -- lowest inactive bg
              (0x28,0x2c,0x34) -- highest inactive bg
              (0xc7,0x92,0xea) -- active bg
              (0xc0,0xa7,0x9a) -- inactive fg
              (0x28,0x2c,0x34) -- active fg

  -- gridSelect menu layout
myGridConfig :: p -> GSConfig Window
myGridConfig colorizer = (buildDefaultGSConfig myColorizer)
  { gs_cellheight   = 40 -- old 30
  , gs_cellwidth    = 200 -- old 200
  , gs_cellpadding  = 6
  , gs_originFractX = 0.5
  , gs_originFractY = 0.5
  , gs_font         = myFont
  }

spawnSelected' :: [(String, String)] -> X ()
spawnSelected' lst = gridselect conf lst >>= flip whenJust spawn
  where conf = def -- old : defaultGSConfig

-- Set favorite apps for the spawnSelected'
myAppGrid :: [(String, String)]
myAppGrid = [ ("lf", "st -e lf")
  -- ("Emacs", "emacsclient -c -a ''")
            , ("Firefox", "firefox")
            , ("Signal", "signal-desktop")
            , ("Discord", "discord")
            , ("Evince", "evince")
            , ("JDownloader", "/home/quentin/.jd2/JDownloader2")
            --, ("Anki", "anki")
            , ("Protonmail", "electron-mail")
            , ("Transmission", "transmission-gtk")
            , ("KeePassXC", "keepassxc")
            ]

-----------------------------------------------
-- KEYBINDINGS
-----------------------------------------------
-- I am using the XMonad.Util.EZConfig module which allows keybindings
-- to be written in simpler, emacs-like format.
myKeys :: [(String, X ())]
myKeys =
  -- XMonad
  [ ("M-C-r", spawn "xmonad --recompile")      -- Recompiles xmonad
  , ("M-z", spawn "xmonad --restart")        -- Restarts xmonad
  , ("M-S-q", io exitSuccess)                  -- Quits xmonad
  , (("M-S-<Esc>"), spawnSelected'
    [ ("Éteindre", "systemctl poweroff -i ")
    , ("Redémarrer", "systemctl reboot")
    , ("Verrouiller", "i3lock -c 000000 && sleep 1")
    ])

  -- Open my preferred terminal
  , ("M-<Return>", spawn myTerminal)

  -- Run Prompt
  , ("M-S-<Return>", spawn "dmenu_run -i -p \"Run: \"") -- Dmenu

  -- Other Dmenu Prompts
  , ("M-p c", spawn "~/.local/bin/dmcolors")  -- pick color from our scheme
  , ("M-p e", spawn "~/.local/bin/dmconf")  -- edit config file
  , ("M-p i", spawn "~/.local/bin/dmscrot")  -- screenshots
  , ("M-p k", spawn "~/.local/bin/dmkill")  -- kill processes

  -- Kill windows
  , ("M-S-a", kill1)                           -- Kill the currently focused client
  , ("M-S-c", killAll)                         -- Kill all the windows on current workspace

  -- Floating windows
  , ("M-S-t", withFocused $ windows . W.sink)  -- Push floating window back to tile.
  , ("M-S-t", sinkAll)                  -- Push ALL floating windows back to tile.

  -- Grid select
  , ("M-t", spawnSelected' myAppGrid)
  , ("M-S-g", goToSelected $ myGridConfig myColorizer)
  , ("M-S-b", bringSelected $ myGridConfig myColorizer)

  -- Window navigation
  , ("M-m", windows W.focusMaster)             -- Move focus to the master window
  , ("M-s", windows W.focusDown)               -- Move focus to the next window
  , ("M-r", windows W.focusUp)                 -- Move focus to the prev window
  -- , ("M-S-m", windows W.swapMaster)            -- Swap the focused window and the master window
  , ("M-S-s", windows W.swapDown)              -- Swap the focused window with the next window
  , ("M-S-r", windows W.swapUp)                -- Swap the focused window with the prev window
  , ("M-<Backspace>", promote)                 -- Moves focused window to master, all others maintain order
  , ("M1-S-<Tab>", rotSlavesDown)              -- Rotate all windows except master and keep focus in place
  , ("M1-C-<Tab>", rotAllDown)                 -- Rotate all the windows in the current stack
  , ("M-S-s", windows copyToAll)
  , ("M-C-s", killAllOtherCopies)

  , ("M-C-M1-<Up>", sendMessage Arrange)
  , ("M-C-M1-<Down>", sendMessage DeArrange)
  , ("M-<Up>", sendMessage (MoveUp 10))             --  Move focused window to up
  , ("M-<Down>", sendMessage (MoveDown 10))         --  Move focused window to down
  , ("M-<Right>", sendMessage (MoveRight 10))       --  Move focused window to right
  , ("M-<Left>", sendMessage (MoveLeft 10))         --  Move focused window to left
  , ("M-S-<Up>", sendMessage (IncreaseUp 10))       --  Increase size of focused window up
  , ("M-S-<Down>", sendMessage (IncreaseDown 10))   --  Increase size of focused window down
  , ("M-S-<Right>", sendMessage (IncreaseRight 10)) --  Increase size of focused window right
  , ("M-S-<Left>", sendMessage (IncreaseLeft 10))   --  Increase size of focused window left
  , ("M-C-<Up>", sendMessage (DecreaseUp 10))       --  Decrease size of focused window up
  , ("M-C-<Down>", sendMessage (DecreaseDown 10))   --  Decrease size of focused window down
  , ("M-C-<Right>", sendMessage (DecreaseRight 10)) --  Decrease size of focused window right
  , ("M-C-<Left>", sendMessage (DecreaseLeft 10))   --  Decrease size of focused window left

  -- Layouts
  , ("M-<Tab>", sendMessage NextLayout)                              -- Switch to next layout
  , ("M-S-<Space>", sendMessage ToggleStruts)                          -- Toggles struts
  , ("M-S-n", sendMessage $ Toggle NOBORDERS)                          -- Toggles noborder
  , ("M-f", sendMessage (Toggle NBFULL) >> sendMessage ToggleStruts) -- Toggles noborder/full
  , ("M-S-f", sendMessage (T.Toggle "float"))
  , ("M-S-x", sendMessage $ Toggle REFLECTX)
  , ("M-S-y", sendMessage $ Toggle REFLECTY)
  , ("M-S-m", sendMessage $ Toggle MIRROR)

  -- Increase/decrease windows in the master pane or the stack
  , ("M-S-<Up>", sendMessage (IncMasterN 1))   -- Increase number of clients in the master pane
  , ("M-S-<Down>", sendMessage (IncMasterN (-1)))  -- Decrease number of clients in the master pane
  , ("M-C-<Up>", increaseLimit)              -- Increase number of windows that can be shown
  , ("M-C-<Down>", decreaseLimit)                -- Decrease number of windows that can be shown

  -- Window resizing
  --, ("M-C-t", sendMessage Shrink)
  --, ("M-C-n", sendMessage Expand)
  --, ("M-C-s", sendMessage MirrorShrink)
  --, ("M-C-r", sendMessage MirrorExpand)
  , ("M-S-;", sendMessage zoomReset)
  , ("M-;", sendMessage ZoomFullToggle)

  -- Sublayouts
  -- This is used to push windows to tabbed sublayouts, or pull them out of it
  , ("M-C-t", sendMessage $ pullGroup L)
  , ("M-C-n", sendMessage $ pullGroup R)
  , ("M-C-r", sendMessage $ pullGroup U)
  , ("M-C-s", sendMessage $ pullGroup D)
  , ("M-C-m", withFocused (sendMessage . MergeAll))
  , ("M-C-/", withFocused (sendMessage . UnMergeAll))
  , ("M-C-.", onGroup W.focusUp')	-- switch focus to next tab
  , ("M-C-,", onGroup W.focusDown')	-- switch focus to prev tab


  -- Workspaces
  , ("M-S-<KP_Add>", shiftTo Next nonNSP >> moveTo Next nonNSP)       -- Shifts focused window to next workspace
  , ("M-S-<KP_Subtract>", shiftTo Prev nonNSP >> moveTo Prev nonNSP)  -- Shifts focused window to previous workspace

  -- Scratchpads
  , ("M-C-v" , scratchpadSpawnActionTerminal myTerminal)
  , ("M-C-<Return>", namedScratchpadAction myScratchPads "terminal")
  , ("M-C-c", namedScratchpadAction myScratchPads "cmus")

  -- Emacs
  , ("M-e e", spawn "emacsclient -c -a ''")                           -- start emacs
  , ("M-e a", spawn "emacsclient -c -a '' --eval '(emms)'")           -- emms emacs audio player
  , ("M-e b", spawn "emacsclient -c -a '' --eval '(ibuffer)'")        -- list emacs buffers
  , ("M-e d", spawn "emacsclient -c -a '' --eval '(dired nil)'")      -- dired emacs file manager
  , ("M-e m", spawn "emacsclient -c -a '' --eval '(mu4e)'")           -- mu4e emacs email client
  , ("M-e n", spawn "emacsclient -c -a '' --eval '(elfeed)'")         -- elfeed emacs rss client
  , ("M-e s", spawn "emacsclient -c -a '' --eval '(eshell)'")         -- eshell within emacs
  , ("M-e t", spawn "emacsclient -c -a '' --eval '(+vterm/here nil)'")         -- eshell within emacs

  -- Applications
  -- Multimedia keys
  , ("<XF86AudioMute>", spawn "amixer set Master mute")
  , ("<XF86AudioLowerVolume>", spawn "amixer set Master 5%- unmute")
  , ("<XF86AudioRaiseVolume>", spawn "amixer set Master 5%+ unmute")
  , ("<XF86MonBrightnessUp>", spawn "sudo /home/quentin/.local/bin/perm/brightness.sh +")
  , ("<XF86MonBrightnessDown>", spawn "sudo /home/quentin/.local/bin/perm/brightness.sh -")

  , ("M-<Print>", spawn "flameshot gui")
  ]
  where nonNSP          = WSIs (return (\ws -> W.tag ws /= "nsp"))
        nonEmptyNonNSP  = WSIs (return (\ws -> isJust (W.stack ws) && W.tag ws /= "nsp"))

------------------------------------------
-- WORKSPACES
------------------------------------------
myWorkspaces :: [String]
myWorkspaces = ["1", "2", "3", "4", "5", "6", "7", "8", "9"]

myWorkspaceIndices = M.fromList $ zipWith (,) myWorkspaces [1..9]

-- Problem when compiling (indentation ?)
clickable ws = "<action=xdotool key super+" ++ show i ++ ">" ++ ws ++ "</action>"
  where i = fromJust $ M.lookup ws myWorkspaceIndices

-- clickable :: [(WorkspaceId, Int)] -> WorkspaceId -> String
-- clickable ws w = fromMaybe w $ (\x -> xmobarAction ("xdotool key=super+" ++ show x) "1" w) <$> lookup w ws

-- Theme for showName which prints current workspace when moving
myShowWNameTheme :: SWNConfig
myShowWNameTheme = def
	{ swn_font	= "xft:Ubuntu:bold:size=60"
	, swn_fade	= 1.0
	, swn_bgcolor	= "#1c1f24"
	, swn_color	= "#ffffff"
	}


------------------------------------------
-- LAYOUTS
------------------------------------------
myLayoutHook = smartBorders . avoidStruts $ mouseResize $ windowArrange $ T.toggleLayouts floats $
               mkToggle (NBFULL ?? NOBORDERS ?? EOT) $ myDefaultLayout
  where
    -- myDefaultLayout = tall ||| grid ||| threeCol ||| threeRow ||| oneBig ||| noBorders monocle ||| space ||| floats
    myDefaultLayout = tall ||| oneBig ||| floats ||| threeRow ||| tabs

    tall       = renamed [Replace "tall"]
		$ addTabs shrinkText myTabTheme
		$ subLayout [] Simplest --(smartBorders Simplest)
		$ limitWindows 12
		$ ResizableTall 1 (3/100) (1/2) []
    -- possibilité d'écrire : $ spacing 6 avant $ ResizableTall pour rajouter de l'espacement entre les fenêtres
    oneBig     = renamed [Replace "oneBig"]
    		$ limitWindows 6
		$ Mirror
		$ mkToggle (single MIRROR)
		$ mkToggle (single REFLECTX)
		$ mkToggle (single REFLECTY)
		$ OneBig (5/9) (8/12)
    floats     = renamed [Replace "floats"]
    		$ smartBorders
    		$ limitWindows 20 simplestFloat
    threeRow   = renamed [Replace "threeRow"]
    		$ limitWindows 3
		$ Mirror
		$ mkToggle (single MIRROR) zoomRow
    tabs     = renamed [Replace "tabs"]
    		$ tabbed shrinkText myTabTheme

-- setting colors for tabs layout and tabs sublayout.
myTabTheme = def { fontName		= myFont
		 , activeColor		= "#46d9ff"
		 , inactiveColor	= "#313846"
		 , activeBorderColor	= "#46d9ff"
		 , inactiveBorderColor	= "#282c34"
		 , activeTextColor	= "#282c34"
		 , inactiveTextColor	= "#d0d0d0"
		 }


--------------------------------------------------
-- SCRATCHPADS
--------------------------------------------------
myScratchPads = [ NS "terminal" spawnTerm findTerm manageTerm
                , NS "cmus" spawnCmus findCmus manageCmus
                ]
  where
    spawnTerm = myTerminal ++ "-n scratchpad"
    findTerm = resource =? "scratchpad"
    manageTerm = customFloating $ W.RationalRect l t w h
      where
        h = 0.9
        w = 0.9
        t = 0.95 - h
        l = 0.95 - w
    spawnCmus = myTerminal ++ "-n cmus 'cmus'"
    findCmus = resource =? "cmus"
    manageCmus = customFloating $ W.RationalRect l t w h
      where
        h = 0.9
        w = 0.9
        t = 0.95 - h
        l = 0.95 - w

manageScratchPad :: ManageHook
manageScratchPad = scratchpadManageHook (W.RationalRect l t w h)

  where

    h = 0.1     -- terminal height, 10%
    w = 1       -- terminal width, 100%
    t = 1 - h   -- distance from top edge, 90%
    l = 1 - w   -- distance from left edge, 0%


-------------------------------------------------------
-- NOTIFICATIONS
-------------------------------------------------------
data LibNotifyUrgencyHook = LibNotifyUrgencyHook deriving (Read, Show)

instance UrgencyHook LibNotifyUrgencyHook where
  urgencyHook LibNotifyUrgencyHook w = do
    name <- getName w
    Just idx <- fmap (W.findTag w) $ gets windowset

    safeSpawn "notify-send" [show name, "workspace" ++ idx]


---------------------------------------------------------------

  -- Whether focus follows the mouse pointer.
myFocusFollowsMouse :: Bool
myFocusFollowsMouse = True

-- Whether clicking on a window to focus also passes the click to the window
myClickJustFocuses :: Bool
myClickJustFocuses = False

-- Border colors for unfocused and focused windows, respectively.
--
myNormalBorderColor  = "#dddddd"
myFocusedBorderColor = "#ff0000"

------------------------------------------------------------------------
-- Mouse bindings: default actions bound to mouse events
--
myMouseBindings (XConfig {XMonad.modMask = modm}) = M.fromList $

    -- mod-button1, Set the window to floating mode and move by dragging
    [ ((modm, button1), (\w -> focus w >> mouseMoveWindow w
                                       >> windows W.shiftMaster))

    -- mod-button2, Raise the window to the top of the stack
    , ((modm, button2), (\w -> focus w >> windows W.shiftMaster))

    -- mod-button3, Set the window to floating mode and resize by dragging
    , ((modm, button3), (\w -> focus w >> mouseResizeWindow w
                                       >> windows W.shiftMaster))

    -- you may also bind events to the mouse scroll wheel (button4 and button5)
    ]

------------------------------------------------------------------------
-- Window rules:
------------------------------------------------------------------------
myManageHook = composeAll
    [ className =? "MPlayer"        --> doFloat
    , className =? "Gimp"           --> doFloat
    , resource  =? "desktop_window" --> doIgnore
    , resource  =? "kdesktop"       --> doIgnore ]
