Config { font = "xft:Mononoki Nerd Font:pixelsize=12:antialias=true:hinting=true"
       , additionalFonts = [ "xft:FontAwesome:pixelsize=13" ]
       , borderColor = "black"
       , border = TopB
       , bgColor = "#282A36" 
       , fgColor = "grey"
       , alpha = 255
       , position = Top 
       , textOffset = -1
       , iconOffset = -1
       , lowerOnStart = True
       , pickBroadest = False
       , persistent = True
       , hideOnStart = False
       , iconRoot = "/home/quentin/.xmonad/xpm/" -- default : "."
       , allDesktops = True
       , overrideRedirect = True
       , commands = [
       -- Time and date
       -- Run DateZone "<fn=1>\xf133</fn> %A %_d %b %Y (%H:%M:%S)" "fr_FR.UTF8" "Europe/Paris" "date" 5
       Run Com "date" ["+%A %_d %b (%H:%M:%S)"] "date" 5

       -- Network up and down
       --, Run Network "eth0" ["-t", "\xf0aa <rx>kb  \xf0ab <tx>kb"] 10
       , Run Network "enp2s0f1" ["-L","0","-H","32", "--normal","green","--high","red"] 10
       , Run Network "wlp3s0" ["-L","0","-H","32", "--normal","green","--high","red"] 10
       --, Run DynNetwork ["-L","0","-H","32", "--normal","green","--high","red"] 10

       -- CPU usage in percent
       , Run Cpu ["-L","3","-H","50", "--normal","green","--high","red"] 10

       -- RAM used number and percent
       , Run Memory ["-t","Mem: <usedratio>%"] 10
       , Run Swap [] 10

       -- Disk space free
       , Run DiskU [("/", "<fn=1>\xf0a0</fn> <free>"),("/home", "<fn=1>\xf015</fn> <free>")] [] 60

       -- Volume
       -- , Volume "default" "Master" [] 10
       
       -- Prints out the left side items such as layouts, workspaces, etc.
       -- The Workspaces are clickable in my config
       , Run UnsafeStdinReader
       --             , Run Date "%a %_d %b %Y %H:%M:%S" "date" 10
       ]
       , sepChar = "%"
       , alignSep = "}{"
      , template = "<action=`xdotool key control+alt+g`> <icon=haskell_20.xpm/></action> <fc=#666666>|</fc> %UnsafeStdinReader% }\
                    \{ <fc=#FFB86C>%cpu% </fc><fc=#666666>| </fc><fc=#FF5555> %memory% - %swap% </fc><fc=#666666>| </fc><fc=#82AAFF>%disku% </fc><fc=#666666>| </fc><fc=#c3e88d>%enp2s0f1% %wlp3s0%</fc><fc=#666666>| </fc><fc=#666666>| </fc><fc=#ee9a00>%date%</fc> "
		    -- %date%"
       }
