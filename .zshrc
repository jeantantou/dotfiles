# Lines configured by zsh-newuser-install
HISTFILE=~/.histfile
HISTSIZE=1000
SAVEHIST=1000
bindkey -e
# End of lines configured by zsh-newuser-install
# The following lines were added by compinstall
zstyle :compinstall filename '/home/quentin/.zshrc'

# autoload -Uz compinit
# compinit
# # End of lines added by compinstall

setopt prompt_subst
autoload -Uz compinit && compinit

# Pour la partie sur Git :
# https://stackoverflow.com/questions/1128496/to-get-a-prompt-which-indicates-git-branch-in-zsh
parse_git_branch() {
    git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/ \1/'
}

PROMPT='[%F{cyan}%n%f %B%F{blue}%~%f%b%F{red}$(parse_git_branch)%f]$ '

# export PS1="[%6(D.%30(d.%F{red}Joyeux anniversaire %b.).)%F{cyan}%n%f  %B%F{blue}%~%f%b]$ "
export PATH="$HOME/.cargo/bin:/snap/bin:$HOME/.local/bin:$HOME/.local/share/lf:$PATH"
export XDG_CURRENT_DESKTOP=i3
export RANGER_LOAD_DEFAULT_RC=FALSE
export BROWSER=/opt/firefox/firefox

alias adventure='emacs -batch -l dunnet'
alias bk='cd $OLDPWD'
alias cinetek='/home/quentin/.local/bin/kinow* --no-sandbox'
alias clean='rm -f *~ 2> /dev/null; rm -f .*~ 2> /dev/null'
alias config='/usr/bin/git --git-dir=$HOME/.cfg/ --work-tree=$HOME'
alias dus='du -sckx * | sort -nr'
alias ext='/home/quentin/.scripts/extract.sh'
alias i3conf='emacsclient ~/.config/i3/config'
alias l='ls -CF'
alias la='ls -A'
alias lh='ls -a | egrep "^\."'
alias ll='ls -lh'
alias ls='ls --color=auto'
alias mailsize='du -hs ~/Library/mail'
alias octave='octave -no-gui'
alias starwars='telnet towel.blinkenlights.nl'
alias ttop='top -ocpu -R -F -s 2 -n30'
alias yt2mp3='youtube-dl -t -x -audio-format mp3 --audio-quality 0'
alias yt2mp4='youtube-dl -t -f mp4'
alias ytupdate='youtube-dl -U'

# Alias de déplacement
alias agreg='cd ~/Documents/École/Agrégation'
alias fac='cd ~/Documents/École/M1'
alias ordi='cd ~/Documents/Informatique'
alias prog='cd ~/Documents/Informatique/Programmation'
alias tél='cd ~/Téléchargements/JDownloader'

# Icônes pour lf
#export LF_ICONS="di=📁:\
#fi=📃:\
#tw=🤝:\
#ow=📂:\
#ln=⛓:\
#or=❌:\
#ex=🎯:\
#*.txt=✍:\
#*.mom=✍:\
#*.me=✍:\
#*.ms=✍:\
#*.png=🖼:\
#*.webp=🖼:\
#*.ico=🖼:\
#*.jpg=📸:\
#*.jpe=📸:\
#*.jpeg=📸:\
#*.gif=🖼:\
#*.svg=🗺:\
#*.tif=🖼:\
#*.tiff=🖼:\
#*.xcf=🖌:\
#*.html=🌎:\
#*.xml=📰:\
#*.gpg=🔒:\
#*.css=🎨:\
#*.pdf=📚:\
#*.djvu=📚:\
#*.epub=📚:\
#*.csv=📓:\
#*.xlsx=📓:\
#*.tex=📜:\
#*.md=📘:\
#*.r=📊:\
#*.R=📊:\
#*.rmd=📊:\
#*.Rmd=📊:\
#*.m=📊:\
#*.mp3=🎵:\
#*.opus=🎵:\
#*.ogg=🎵:\
#*.m4a=🎵:\
#*.flac=🎼:\
#*.wav=🎼:\
#*.mkv=🎥:\
#*.mp4=🎥:\
#*.webm=🎥:\
#*.mpeg=🎥:\
#*.avi=🎥:\
#*.mov=🎥:\
#*.mpg=🎥:\
#*.wmv=🎥:\
#*.m4b=🎥:\
#*.flv=🎥:\
#*.zip=📦:\
#*.rar=📦:\
#*.7z=📦:\
#*.tar.gz=📦:\
#*.z64=🎮:\
#*.v64=🎮:\
#*.n64=🎮:\
#*.gba=🎮:\
#*.nes=🎮:\
#*.gdi=🎮:\
#*.1=ℹ:\
#*.nfo=ℹ:\
#*.info=ℹ:\
#*.log=📙:\
#*.iso=📀:\
#*.img=📀:\
#*.bib=🎓:\
#*.ged=👪:\
#*.part=💔:\
#*.torrent=🔽:\
#*.jar=♨:\
#*.java=♨:\
#"
