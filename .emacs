;; CONFIGURATION D'EMACS


;; Added by Package.el.  This must come before configurations of
;; installed packages.  Don't delete this line.  If you don't want it,
;; just comment it out by adding a semicolon to the start of the line.
;; You may delete these explanatory comments.


;; Configuration de base
;; https://blog.aaronbieber.com/2015/05/24/from-vim-to-emacs-in-fourteen-days.html
(require 'package)

;;(add-to-list 'package-archives '("org" . "http://orgmode.org/elpa"))
(add-to-list 'package-archives '("melpa" . "http://melpa.org/packages/"))
(add-to-list 'package-archives '("melpa-stable" . "http://stable.melpa.org/packages/"))

(setq package-enable-at-startup nil)
(package-initialize)

(toggle-scroll-bar -1)
(tool-bar-mode -1)

;; (unless (package-installed-p 'use-package)
;;   (package-refresh-contents)
;;   (package-install 'use-package))

;; (eval-when-compile
;;   (require 'use-package))

;; Evil-mode and remaps
;; (use-package evil
;;   :config
;;   (define-key evil-normal-state-map "t" 'evil-backward-char)
;;   (define-key evil-normal-state-map "s" 'evil-next-line)
;;   (define-key evil-normal-state-map "r" 'evil-previous-line)
;;   (define-key evil-normal-state-map "n" 'evil-forward-char))

;; (require 'evil)
;; (evil-mode t)

(global-font-lock-mode t)
(setq font-lock-maximum-decoration t)

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(ansi-color-faces-vector
   [default default default italic underline success warning error])
 '(custom-enabled-themes (quote (tango-dark)))
 '(fill-column 70)
 '(inhibit-startup-screen t)
 '(linum-format (quote linum-relative))
 '(package-selected-packages
   (quote
    (evil-visual-mark-mode linum-relative use-package evil auctex sml-mode context-coloring)))
 '(pylint-alternate-pylint-command "pylint3")
 '(python-shell-interpreter "python3")
 '(safe-local-variable-values (quote ((TeX-master . t))))
 '(tool-bar-mode nil))

(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )

;; Pour XeTeX par défaut
(setq-default TeX-engine 'xetex)
(add-hook 'LaTeX-mode-hook 'auto-fill-mode) ;; auto-fill-mode dans Auctex
(add-hook 'LaTeX-mode-hook 'reftex-mode) ;; reftex dans Auctex

;; Pour Org-mode
(require 'org)
(define-key global-map "\C-cl" 'org-store-link)
(define-key global-map "\C-ca" 'org-agenda)
(setq org-log-done t)
(add-hook 'org-mode-hook 'auto-fill-mode)
