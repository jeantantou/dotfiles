" Pour avoir des raccourcis adaptés au bépo
if !empty(system("setxkbmap -print|grep bepo"))
        source ~/.vimrc.bepo
endif

set autoindent
set tabstop=4
set expandtab
